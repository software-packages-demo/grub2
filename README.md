# grub2

GRand Unified Bootloader, version 2. https://gnu.org/software/grub

# Official documentation
## Manual
* [*GNU GRUB Manual*](https://gnu.org/software/grub/manual/grub/html_node/)
  (2019)

### Pager
* [*pager*
  ](https://gnu.org/software/grub/manual/grub/html_node/pager.html)

# Unofficial documentation
* [*Configuring / choosing non-default or older kernel to boot*](https://wiki.debian.org/Grub2#Configuring_.2F_choosing_non-default_or_older_kernel_to_boot)
* [*Boot FreeBSD ISO from Grub2 USB stick*
  ](https://forums.freebsd.org/threads/boot-freebsd-iso-from-grub2-usb-stick.19701/)
  2010-11-26 FreeBSD Forums

# Booting from ISO (CD/DVD/...) images on hard drive
* [grub 2 iso cd images launching](https://google.com/search?q=grub+2+iso+cd+images+launching)
* [* Grub2/ISOBoot*](https://help.ubuntu.com/community/Grub2/ISOBoot)
  * [* Examples*](https://help.ubuntu.com/community/Grub2/ISOBoot/Examples)
  * ([fr](https://doc.ubuntu-fr.org/tutoriel/grub2_lancer_des_images_iso))

## loopback.cfg
* [*Loopback.cfg*](http://www.supergrubdisk.org/wiki/Loopback.cfg)
  (2012) supergrubdisk wiki

## Why chainloading the CD loader from grub is very difficult
* [*grub chainloader isolinux*
  ](https://google.com/search?q=grub+chainloader+isolinux)
